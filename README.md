## Chrome extension to encrypt files before uploading it to server

#Steps to install extension in chrome browser
1. Clone this repository on your local machine.
2. Open chrome browser and open `chrome://extensions/`.
3. Turn on the `Developers` mode by clicking on the toggle button at right top of chrome browser.
4. Click on `Load Unpacked` option, it will open window dialog. Select the folder where you have cloned the repo.

Once done, your plugin is ready to work now in chrome.

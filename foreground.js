console.log("Foreground script started");
const parentForms = [];
const elements = document.querySelectorAll('[data-obliv-encrypt]'); //select all html elements with attribute data-obliv-encrypt
const body = document.querySelector('body');
console.log(elements)

// iterate through each element and add change event listener to get updated files
elements.forEach(element => {
    console.log(element);
    element.addEventListener('change', (event) => {
        addLoader();
        if (element.files.length > 0) {
            let file = event.target.files[0];
            console.log(file);
            // fetch the encryption key from the api
            fetch('http://localhost:3000/encryptionKey/' + element.dataset.oblivEncrypt)
                .then(response => {
                    return response.json();
                })
                .then(jsonObj => {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        console.log(e);
                        let data = e.target.result;
                        let iv = crypto.getRandomValues(new Uint8Array(16));
                        importKey(jsonObj)
                        .then((result) => {
                            console.log(result);
                            crypto.subtle.encrypt({ name: "AES-GCM", iv: iv }, result, data)
                            .then(res => {
                                console.log(res);
                                let blob = new Blob([res], {type: file.type});
                                downloadEncryptedFile(blob, 'encrypted-'+file.name);
                                console.log(blob);
                                let dataTransfer = new DataTransfer();
                                let encryptedFile = new File([blob], 'encrypted-'+file.name);
                                dataTransfer.items.add(encryptedFile);
                                element.files = dataTransfer.files;
                                console.log(encryptedFile);
                            });
                        })
                        .catch(err => {
                            console.error(err);
                        })
                        .finally(() => {
                            stopLoader();
                        });
                    }
                    reader.readAsArrayBuffer(file);
                });
            }
        });

    if (element.form && !parentForms.includes(element.form)) {
        const parentForm = element.form;
        parentForms.push(parentForm);
        parentForm.addEventListener("submit", () => {
            // event.preventDefault();
            console.log("form submitted");
        });
    }
});

/**
 * @description - function to append loader on page when file is being processed
 */
function addLoader() {
    const loaderParent = document.createElement('div');
    loaderParent.id = 'loadingDiv';
    const loader = document.createElement('div');
    loader.id = 'loader';
    loaderParent.appendChild(loader);
    body.appendChild(loaderParent);
}

/**
 * @description - function to remove loader from page when file is completely processed
 */
function stopLoader() {
    body.removeChild(document.getElementById('loadingDiv'));
}

/**
 * @description - function to get key from the JWK object 
 * @param {*} rawKey - encryption JWK object
 * @returns = encryption key
 */
async function importKey(rawKey) {
    var key = await crypto.subtle.importKey(
        "jwk",
        rawKey,
        { name: "AES-GCM" },
        true,
        ["decrypt", "encrypt"]
    );
    return key;
}

/**
 * @description - function to download the encrypted file 
 */
function downloadEncryptedFile(blob, fileName) {
    const url = URL.createObjectURL(blob);
    // download the file
      const a = document.createElement('a');
      a.href = url;
      a.download = fileName;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
  }

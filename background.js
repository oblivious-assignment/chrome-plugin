console.log("from background")

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.status === 'complete' && /^http/.test(tab.url)) {
        chrome.scripting.executeScript({
            target: { tabId: tabId },
            files: ["./foreground.js"]
        }).then(() => {
            console.log("INJECTED THE SCRIPT.");
        }).catch(err => console.log(err));

        chrome.scripting.insertCSS({
            target: { tabId: tabId },
            files: ["./styles.css"]
        })
        .then(() => {
            console.log("INJECTED THE STYLES.");
        })
        .catch(err => console.log(err));
    }
});